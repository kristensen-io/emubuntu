This container downloads a disk image, boots it as a QEMU machine and bootstraps it using cloud-init, which makes it accessible by SSH from the container.

### Usage

Registry: registry.kristensen.io/kristensenio/qemu-container

To access the QEMU machine from the container, use `ssh -o SendEnv="*" -o StrictHostKeyChecking=no -i /root/qemu_ssh_key ubuntu@localhost -p2222`

If used in the Gitlab Kubernetes executor, then use this variable to make sure that the entrypoint is executed: `FF_KUBERNETES_HONOR_ENTRYPOINT: 1`

An example of using the container in your `.gitlab-ci.yml` could look something like this:

~~~
  variables:
    FF_KUBERNETES_HONOR_ENTRYPOINT: 1
  script:
    - echo "Look mom, I am inside a container!"
    - |
        i=1
        while ! ssh -o StrictHostKeyChecking=no -i /root/qemu_ssh_key localhost -p2222 exit 0; do
          if [ $i -gt 30 ]; then
            echo "Timed out while waiting for QEMU SSH"
            exit 1
          fi
          echo "Waiting for QEMU SSH - $i"
          sleep 10
          i=$(expr $i + 1)
        done
    - scp -r -o StrictHostKeyChecking=no -i /root/qemu_ssh_key -P2222 /builds localhost:/builds
    - |-
        ssh -o SendEnv="*" -o StrictHostKeyChecking=no -i /root/qemu_ssh_key localhost -p2222 bash <<CMD
          cd $CI_PROJECT_DIR
          echo "Look mom, I am inside a QEMU machine inside a container!"
        CMD
~~~

At the time of writing, you have these ENVs available - feel free to double check:

~~~
# grep '^ENV ' Dockerfile | awk '{print $2}'
DEBIAN_FRONTEND
QEMU_IMAGE_URL
QEMU_DISK_SIZE
QEMU_BUILD_CMD
QEMU_RUNTIME_CMD
~~~

---

### TODO

* Graceful shutdown
* Add knobs for KVM
* Download/use image at runtime if not baked in
