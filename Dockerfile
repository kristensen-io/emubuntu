FROM ubuntu:22.04

# Ensure that apt-get install does not prompt interactively
ENV DEBIAN_FRONTEND noninteractive

# Disable auto installation of recommended packages, too many unwanted packages gets installed without this
RUN apt-config dump | grep -we Recommends -e Suggests | sed s/1/0/ | tee /etc/apt/apt.conf.d/999norecommend

# Install pre-requisites
RUN apt-get update && \
    apt-get install -y \
      qemu-system-arm \
      qemu-system-x86 \
      qemu-efi \
      ovmf \
      swtpm \
      swtpm-tools \
      cloud-utils \
      openssh-client \
      netcat-openbsd \
      socat \
      rsync \
      curl

RUN apt-get clean autoclean

COPY files/user-data.yaml /root/user-data.yaml

COPY files/qemu_ssh_key /root/qemu_ssh_key
RUN chmod 600 /root/qemu_ssh_key

COPY files/entrypoint.py /root/entrypoint.py
RUN chmod +x /root/entrypoint.py

# Create cloud-init iso
RUN cloud-localds /root/cloud-init.iso /root/user-data.yaml

# Build args
ENV QEMU_IMAGE_URL https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64-disk-kvm.img
ENV QEMU_DISK_SIZE 50G

# Download base image
ADD ${QEMU_IMAGE_URL} /root/base-image.qcow2

# Create a qcow2 disk, that uses the base image as backing file, so that we do not change the base image
RUN qemu-img create -f qcow2 -F qcow2 -b /root/base-image.qcow2 /root/read-write.qcow2 ${QEMU_DISK_SIZE}

ENV QEMU_BUILD_CMD qemu-system-x86_64 \
  -cpu max -smp cores=4,threads=1,sockets=1 \
  -m 9660392K \
  -serial mon:stdio -display none \
  -device virtio-blk-pci,drive=root,id=virtblk0,num-queues=4 \
    -drive file=/root/read-write.qcow2,format=qcow2,if=none,id=root \
  -device ahci,id=ahci \
    -cdrom /root/cloud-init.iso \
  -nic user,model=virtio,id=user.0,hostfwd=tcp::2222-:22

ENV QEMU_RUNTIME_CMD qemu-system-x86_64 \
  -cpu max -smp cores=4,threads=1,sockets=1 \
  -m 9660392K \
  -serial unix:/root/qemu-serial.sock,server,nowait -display none \
  -device virtio-blk-pci,drive=root,id=virtblk0,num-queues=4 \
    -drive file=/root/read-write.qcow2,format=qcow2,if=none,id=root \
  -nic user,model=virtio,id=user.0,hostfwd=tcp::2222-:22

# Run cloud-init - it will power off the machine when done
RUN $QEMU_BUILD_CMD

# Run a script that runs QEMU in the background, and a bash pty in the foreground
ENTRYPOINT /root/entrypoint.py
