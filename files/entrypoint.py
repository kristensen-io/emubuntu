#!/usr/bin/python3

# Runs the QEMU machine in the background, and gives you a shell in the foreground

import os
import pty
import subprocess

subprocess.Popen(os.environ["QEMU_RUNTIME_CMD"], shell=True)

pty.spawn("/bin/bash")
